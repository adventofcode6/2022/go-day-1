package main

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

func main() {
	fmt.Println(time.Now())
	state := State{
		index:            0,
		sum:              0,
		currentCallories: 0,
		hasLastNewLine:   false,
		maxCallories:     [3]int{0, 0, 0},
	}
	file, _ := os.Open("./input.txt")

	buffer := bufio.NewReader(file)
	for {
		byte, _, error := buffer.ReadRune()
		if error != nil {
			state = state.end()
			break
		}
		state = state.next(byte)
	}
	total := state.maxCallories[0] + state.maxCallories[1] + state.maxCallories[2]
	fmt.Println(total)
}
