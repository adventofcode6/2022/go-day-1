package main

type State struct {
	index            int
	sum              int
	currentCallories int
	hasLastNewLine   bool
	maxCallories     [3]int
}

func (previous State) end() (state State) {
	state = previous
	if previous.sum > previous.maxCallories[2] {
		state.maxCallories[2] = previous.sum
	}
	if previous.sum > previous.maxCallories[1] {
		state.maxCallories[2] = previous.maxCallories[1]
		state.maxCallories[1] = previous.sum
	}
	if previous.sum > previous.maxCallories[0] {
		state.maxCallories[1] = previous.maxCallories[0]
		state.maxCallories[0] = previous.sum
	}
	return
}

func (previous State) next(rune rune) (state State) {
	state = previous
	switch rune {
	case 10:
		{
			if previous.hasLastNewLine {
				state = previous.end()
				state.hasLastNewLine = false
				state.sum = 0
				break
			}
			state.index += 1
			state.sum += previous.currentCallories
			state.currentCallories = 0
			state.hasLastNewLine = true
			break
		}
	default:
		{
			state.hasLastNewLine = false
			state.currentCallories *= 10
			state.currentCallories += int(rune) - '0'
			break
		}
	}
	return
}
